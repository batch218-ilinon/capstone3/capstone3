import React, {useState, useEffect} from 'react';
import LoadingSpinner from './components/LoadingSpinner'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout'
import Products from './pages/Products'
import Cart from './pages/Cart'
import Orders from './pages/Orders'
import Profile from './pages/Profile'
import AdminDashboard from './pages/AdminDashboard'

import AppNavbar from './components/AppNavbar'
import Footer from './components/Footer'

import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'
import {UserProvider} from './UserContext'



function App() {

  const [loading, setLoading] = useState(false);

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
      fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
          headers: {
              Authorization: `Bearer ${localStorage.getItem('item')}`
          }
      })
      .then(res => res.json())
      .then(data => {
          console.log(data)
          if(typeof data._id !== "undefined") {
              setUser({
                  id: data._id,
                  firstName: data.firstName,
                  lastName: data.lastName,
                  email: data.email,
                  mobileNo: data.mobileNo,
                  isAdmin: data.isAdmin
              })
          } 
          else { 
              setUser({
                  id: null,
                  isAdmin: null
              })
          }

      })
  }, []);
  console.log(process.env.REACT_APP_API_URL)
  return (
    <div className="allComp">
    {(loading) ?
    <LoadingSpinner />
    :
    <UserProvider value={{user, setUser, unsetUser, loading, setLoading}}>
    <Router>
      <AppNavbar />
      <Routes>
        <Route path = "/" element= {<Home />} />
        <Route path = "/register" element= {<Register />} />
        <Route path = "/login" element= {<Login />} />
        <Route path = "/logout" element= {<Logout />} />
        <Route path = "/products" element= {<Products />} />
        <Route path = "/cart" element= {<Cart />} />
        <Route path = "/orders" element= {<Orders />} />
        <Route path = "/profile" element= {<Profile />} />
        <Route path = "/dashboard" element= {<AdminDashboard />} />
      </Routes>
      <Footer />
    </Router>
    </UserProvider>
    }
    </div>
  );
}

export default App;
