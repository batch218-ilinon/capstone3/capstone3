import AdminProduct from './AdminProduct'
import DashboardButton from './DashboardButton'

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {useState, useContext} from 'react';
import UserContext from '../UserContext';

export default function AdminCard() {
	const [products, setProducts] = useState([])
	const {setComponents, setSingleCardComponent} = useContext(UserContext);
	const {setUpdateKey} = useContext(UserContext);

	function createCards(){
		fetch(`${process.env.REACT_APP_API_URL}/product/all`)
		.then(res => res.json())
		.then(data => {
			setProducts(data.map(product => {	
				return <AdminProduct key={product._id} product = {product}/>

			}))
		})
	}

	createCards()

	function returnDashboard(){
	  setUpdateKey(false)	
	  setSingleCardComponent(null)		
	  setComponents(<DashboardButton />)
	}

	return (
		<>
		<Container className="mt-5">
			<Row className="justify-content-end">
			<Button className="backToDashboardButton" onClick={returnDashboard} variant="light" type="button" id="cancelBtn">
			  Back
			</Button>
			</Row>
			<Row>
				{products}
			</Row>
		</Container>
		</>
	)
}