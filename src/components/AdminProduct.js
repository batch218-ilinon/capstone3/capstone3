import imageHolder from '../images/productPhoto.jpg';

import {useContext} from 'react';
import UserContext from '../UserContext';

import Button from 'react-bootstrap/Button';
import Card from 'react-bootstrap/Card';
import Col from 'react-bootstrap/Col';


export default function AdminProduct({product}) {

  const {setKey} = useContext(UserContext);
  const {updateKey} = useContext(UserContext);
  const {returnUpdateProduct} = useContext(UserContext);


  function returnKey(){
    setKey(product._id)
  }

  return (
    (product.isActive) ?
    <Col xs={12} sm={8} md={6} lg={4} xl={3} className="p-3">
    <Card onClick={(returnKey)} style={{ width: '18rem' }}>
      <Card.Img variant="top" src={imageHolder} />
      <Card.Body>
        <Card.Title>{product.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">PHP {product.price}</Card.Subtitle>
        <Card.Text>
          {product.description}
        </Card.Text>
        {(updateKey) ?
        <Button variant="info">Update</Button>
        :
        null}
      </Card.Body>
    </Card>
    </Col>
    :
    <Col xs={12} sm={8} md={6} lg={4} xl={3} className="p-3">
    <Card onClick={returnKey} bg={'dark'} text={'white'} style={{ width: '18rem' }}>
      <Card.Img variant="top" src={imageHolder} />
      <Card.Body>
        <Card.Title>{product.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted">PHP {product.price}</Card.Subtitle>
        <Card.Text>
          {product.description}
        </Card.Text>
        {(updateKey) ?
        <Button onClick={(returnKey)} variant="info">Update</Button>
        :
        null}
      </Card.Body>
    </Card>
    </Col>
  );
}
