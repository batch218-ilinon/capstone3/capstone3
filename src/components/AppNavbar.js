import Container from 'react-bootstrap/Container';

import { useContext } from 'react';

import { Link, NavLink } from 'react-router-dom'
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';

import UserContext from '../UserContext';

export default function AppNavbar() {

    const {user} = useContext(UserContext);

  return (
    <>
    {(user.isAdmin === true) ?
    <Navbar sticky="top" className="naviBar" variant="dark" expand="lg">
      <Container className="d-none d-lg-block">
        <Nav className="justify-content-around">
          <Navbar.Brand as={Link} to="/dashboard">(-_-)</Navbar.Brand>
          <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
            <NavDropdown title={`${user.firstName[0]}.${user.lastName}`} id="basic-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="/profile">My account</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
            </NavDropdown>
        </Nav>
      </Container>
      <Container className="d-lg-none">
        <Navbar.Brand as={Link} to="/dashboard">(-_-)</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
              <NavDropdown title={`${user.firstName[0]}.${user.lastName}`} id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/profile">My account</NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
              </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>  
    :
    <Navbar sticky="top" className="naviBar" variant="dark" expand="lg">
      <Container className="d-none d-lg-block">
        <Nav className="justify-content-around">
          <Navbar.Brand as={Link} to="/">(-_-)</Navbar.Brand>
          <Nav.Link as={NavLink} to="/">Home</Nav.Link>
          <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
          <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
          {(user.id !== null) ?
            <NavDropdown title={`${user.firstName[0]}.${user.lastName}`} id="basic-nav-dropdown">
              <NavDropdown.Item as={NavLink} to="/profile">My account</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/orders">My purchase</NavDropdown.Item>
              <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
            </NavDropdown>
            :
            <>
            <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
            <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
            </>
          }
        </Nav>
      </Container>
      <Container className="d-lg-none">
        <Navbar.Brand as={Link} to="/">(-_-)</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/">Home</Nav.Link>
            <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
            <Nav.Link as={NavLink} to="/cart">Cart</Nav.Link>
            {(user.id !== null) ?
              <NavDropdown title={`${user.firstName[0]}.${user.lastName}`} id="basic-nav-dropdown">
                <NavDropdown.Item as={NavLink} to="/profile">My account</NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/orders">My purchase</NavDropdown.Item>
                <NavDropdown.Item as={NavLink} to="/logout">Logout</NavDropdown.Item>
              </NavDropdown>
              :
              <>
              <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
              <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
              </>
            }
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    }
    </>
  );
}
