import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext';

export default function CartBody({product}) {

  const [subTotal, setSubTotal] = useState(product.subTotal);
  const [count, setCount] = useState(product.quantity);
  const { setCartRefresh, updateTotalAmount } = useContext(UserContext)

  function addCount(){
    setCount(count + 1)
    setSubTotal(subTotal + product.price)
    updateTotalAmount(product.price)
  }

  function subtractCount(){
    if(count > 1){
      setCount(count - 1)
      setSubTotal(subTotal - product.price)
      updateTotalAmount(-product.price)
    }
  }

  useEffect(() => {

    fetch(`${process.env.REACT_APP_API_URL}/cart/${product.prodId}/update`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      },
      body: JSON.stringify({
        "quantity": count
      })
    })
      .then(res => res.json())
      .then(data => {
        if(data){
          console.log(data)
        }else{
          console.log(false)
        }
      })
  }, [subTotal])

  function removeProduct(){
    fetch(`${process.env.REACT_APP_API_URL}/cart/${product.productId}/remove`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      }
    })  
    .then(res => res.json())
    .then(data => {
      console.log(typeof data)
      if(data === true){
        setCartRefresh()
      }
      else{
        console.log(data.message)
      }
    })
  }

  return (
      <tbody>
        <tr>
          <td>{product.name}</td>
          <td>{product.price}</td>
          <td>
          <ButtonGroup aria-label="Basic example" size="sm">
            <Button onClick={addCount} variant="secondary">+</Button>
            {count}
            <Button onClick={subtractCount} variant="secondary">-</Button>
          </ButtonGroup>
          </td>
          <td>{subTotal}</td>
          <td><Button onClick={removeProduct} variant="danger">Remove</Button></td>
        </tr>
      </tbody>
  );
}