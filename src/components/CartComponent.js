import CartBody from './CartBody'
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import { useState, useEffect } from 'react';
import {UserProvider} from '../UserContext'
import Swal from 'sweetalert2';

export default function CartComponent(){

	const [cartItems, setCartItems] = useState([])
	const [totalAmount, setTotalAmount] = useState(0)
	const [isActive, setIsActive] = useState(false);
	const [cartRefresh, setCartRefresh] = useState(false)


	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/cart`, {
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  }
		})
		.then(res => res.json())
		.then(data => {
			if(data === false){
				setCartItems(null)
				setTotalAmount(0)
				setIsActive(false)
			}
			else{
				setIsActive(true)
				setTotalAmount(data.totalAmount)
				setCartItems(data.products.map(product => {
					return <CartBody key = {product.prodId}  product = {product}/>
				}))
			}
		})
	}, [cartRefresh])

	const updateTotalAmount = (price) => {
		setTotalAmount(totalAmount + price)
	}

	function cartCheckout(){
		fetch(`${process.env.REACT_APP_API_URL}/cart/checkout`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  }
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)
			if(data === true){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Order Created',
				  showConfirmButton: false,
				  timer: 1500
				})
				setCartItems(null)
				setTotalAmount(0)
				isActive(false)
			}
			else if(data === false){
				console.log(data)
			}
			else{
				console.log(data.message)
			}
		})
	}

	return (
		<Container className="cartWrapper">
			<Row>
	        <Table className="cartTable">
	          <thead>
	            <tr>
	              <th>Name</th>
	              <th>Price</th>
	              <th>Quantity</th>
	              <th>Subtotal</th>
	              <th></th>
	            </tr>
	          </thead>
	          <UserProvider value={{setCartRefresh, updateTotalAmount}}>
	          {cartItems}
	          </UserProvider>
	          <tbody>
	            <tr>
	              <td colSpan={4}>
	              	{ isActive ? 
	              	<Button onClick={cartCheckout} variant="success" size="lg">
	              	Checkout
	              	</Button>
	              	:
	              	<Button variant="success" size="lg" disabled>
	              	Checkout
	              	</Button>}
	              </td>
	              <td colSpan={4}>PHP {totalAmount}</td>
	            </tr>
	          </tbody>
	        </Table>
	        </Row>
	    </Container>
	);
}