import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2';
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

export default function ChangePassComponent(){

	const [password1, setPassword1] = useState("");
	const [newPassword1, setNewPassword1] = useState("");
	const [newPassword2, setNewPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);
	const [matchMessage, setMatchMessage] = useState("");

	const {confirmChange} = useContext(UserContext);

	useEffect(() => {
	    if(newPassword1 !== newPassword2){
	    	setMatchMessage("Password and Confirm password are not matched")
	    }
	    else{
	    	setMatchMessage("")
	    }

	}, [newPassword2])

	useEffect(() => {
	    if((password1 !== '' && newPassword1 !== '' && newPassword2 !== '') && (newPassword1 === newPassword2)){
	                setIsActive(true);
	            } else {
	                setIsActive(false);
	            }

	}, [password1, newPassword1, newPassword2])

	function changePassword(e){
		e.preventDefault()
		confirmChange()
		fetch(`${process.env.REACT_APP_API_URL}/users/changePass`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  },
		  body: JSON.stringify({
		    "password": password1,
		    "newPassword": newPassword1
		  })
		})
		.then(res => res.json())
		.then(data => {
			if(data){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Password change successful',
				  showConfirmButton: false,
				  timer: 1500
				})
			}else{
				Swal.fire({
				  position: 'center',
				  icon: 'error',
				  title: 'Error',
				  showConfirmButton: false,
				  timer: 1500
				})
			}
		})
	}

	function cancelChangePass(){
		confirmChange()
	}

	return (
		<>
		  <h5 className="changePassHeading m-5 pt-3 pb-5 border-bottom border-light">Change Password</h5>
		  <Row className="justify-content-center">
		  <Col xs={8}>

		  <Form onSubmit={changePassword}>
		  <Form.Group className="pt-3" controlId="userPassword1">
		    <Form.Label >Password</Form.Label>
		    <Form.Control type="password" placeholder="Enter password" 
		    value={password1}
		    onChange={e => setPassword1(e.target.value)} 
		    required/>
		  </Form.Group>

		  <Form.Group className="pt-3" controlId="newPassword1">
		    <Form.Label >New Password</Form.Label>
		    <Form.Control type="password" placeholder="Enter new password" 
		    value={newPassword1}
		    onChange={e => setNewPassword1(e.target.value)} 
		    required/>
		  </Form.Group>

		  <Form.Group className="pt-3" controlId="newPassword2">
		    <Form.Label >Confirm Password</Form.Label>
		    <Form.Control type="password" placeholder="Confirm password" 
		    value={newPassword2}
		    onChange={e => setNewPassword2(e.target.value)} 
		    required/>
		  </Form.Group>

		  <Form.Text className="text-muted">
		    {matchMessage}
		  </Form.Text>

		    <Row className="justify-content-center">
		    { isActive ? 
		    <Button className="m-5" variant="primary" type="submit" id="submitBtn">
		    Chage Password
		    </Button>
		    :
		    <Button className="my-5" variant="primary" type="submit" id="submitBtn" disabled>
		    Chage Password
		    </Button>
		    }
		    <Button onClick={confirmChange} variant="danger" type="button" id="cancelBtn">
		    Cancel
		    </Button>
		    </Row>
		  </Form>
		  </Col>
		  </Row>
		</>
	)
}