import DashboardButton from './DashboardButton'

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Swal from 'sweetalert2';

export default function CreateProduct() {

	const [productName, setProductName] = useState("");
	const [productDescription, setProductDescription] = useState("");
	const [productPrice, setProductPrice] = useState("");
	const [productFeatures, setProductFeatures] = useState("");

	const [isActive, setIsActive] = useState(false);

	const {setComponents} = useContext(UserContext);

	useEffect(() => {
	    if(productName !== '' && productDescription !== '' && productPrice !== '' && productFeatures !== ''){
	                setIsActive(true);
	            } else {
	                setIsActive(false);
	            }

	    }, [productName, productDescription, productPrice])

	function createProduct(e){
		e.preventDefault()
		fetch(`${process.env.REACT_APP_API_URL}/product/register`, {
		  method: 'POST',
		  headers: {
		  	'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  },
		  body: JSON.stringify({
		    "name": productName,
		    "description": productDescription,
		    "price": productPrice,
		    "features": productFeatures
		  })
		})
		.then(res => res.json())
		.then(data => {
			if(data === true){
				Swal.fire({
				title: "Success",
				icon: "success",
				text: "Product successfully created!"
				});
				setProductName("");
				setProductDescription("");
				setProductPrice("");
				setProductFeatures("");
			}
			else if(data === false){
				Swal.fire({
				title: "Error",
				icon: "error",
				text: "Error"
				})
			}
			else{
				Swal.fire({
				title: "Error",
				icon: "error",
				text: "Error"
				})
			}
		})
	}

	function cancelCreateProduct(){
		setComponents(<DashboardButton />)
	}

  return (
  	<Container className="createFrom">
  	<h3 className="createFormHeading m-5 pt-3 pb-5 border-bottom border-light">Create Product</h3>
  	<Row className="justify-content-center">
  	<Col xs={8}>
    <Form onSubmit={createProduct}>
      <Form.Group className="mb-3" controlId="productName">
        <Form.Label className="createFormLabel">Product Name</Form.Label>
        <Form.Control type="name" placeholder="Enter product name" 
        value={productName}
        onChange={e => setProductName(e.target.value)} 
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="productDescription">
        <Form.Label className="createFormLabel">Product Description</Form.Label>
        <Form.Control type="description" placeholder="Enter Description" 
        value={productDescription}
        onChange={e => setProductDescription(e.target.value)} 
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="productDescription">
        <Form.Label className="createFormLabel">Product Features</Form.Label>
        <Form.Control rows="7" as="textarea" aria-label="With textarea" placeholder="Enter Features" 
        value={productFeatures}
        onChange={e => setProductFeatures(e.target.value)} 
        required/>
      </Form.Group>

      <Form.Group className="mb-3" controlId="productPrice">
        <Form.Label className="createFormLabel">Product Price</Form.Label>
        <Form.Control type="price" placeholder="Enter product price" 
        value={productPrice}
        onChange={e => setProductPrice(e.target.value)} 
        required/>
      </Form.Group>
      <Row>
      { isActive ?
      <Button variant="primary" type="submit" id="submitBtn" className="my-3">
      Create
      </Button>      
      :
      <Button variant="danger" type="submit" id="submitBtn" className="my-3" disabled>
      Create
      </Button>
      }
      <Button onClick={cancelCreateProduct} variant="primary" type="button" id="cancelBtn" className="my-3">
      Cancel
      </Button>
      </Row>
    </Form>
    </Col>
    </Row>
    </Container>
  );
}