import CreateProduct from './CreateProduct'
import AdminCard from './AdminCard'
import OrderList from './OrderList'
import SetAdmin from './SetAdmin'

import {useContext, useState, useEffect} from 'react';
import UserContext from '../UserContext';
import Button from 'react-bootstrap/Button';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Row';

export default function DashboardButton() {

	const {setComponents} = useContext(UserContext);
	const {setUpdateKey} = useContext(UserContext);
 	const [user, setUser] = useState([]);
 	const [token, setToken] = useState(localStorage.getItem('item'));
	useEffect(() => {
		if(token !== null){
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			    headers: {
			        Authorization: `Bearer ${localStorage.getItem('item')}`
			    }
			})
			.then(res => res.json())
			.then(data => {
				console.log(data.isAdmin)
			    setUser({
			        id: data._id,
			        firstName: data.firstName,
			        lastName: data.lastName,
			        email: data.email,
			        mobileNo: data.mobileNo,
			        isAdmin: data.isAdmin
			    })	
			})
		}
	}, [])

	function CreateComponent(){
		setComponents(<CreateProduct />)
	}

	function UpdateComponent(){
		setUpdateKey(true)
		setComponents(<AdminCard />)
	}

	function ProductComponent(){
		setComponents(<AdminCard />)
	}

	function OrderComponent(){
		setComponents(<OrderList />)
	}

	function SetAnAdmin(){
		setComponents(<SetAdmin />)
	}
  return (
  	<div className="d-grid gap-2 dashboardHeading">
  	  <h1 className="m-5 pt-3 pb-5 border-bottom border-light">Welcome Admin {user.firstName}!</h1>
  	  <Container className="dashboardWrapper">
  	  <Row className="justify-content-around my-5">			
  	  <Button onClick={ProductComponent} className="adminButton" variant="success" size="lg">
  	  Products
  	  </Button>
  	  <Button onClick={OrderComponent} className="adminButton" variant="success" size="lg">
  	    Orders
  	  </Button>
  	  </Row>
  	  <Row className="justify-content-around my-5 pt-5"> 
  	  <Button onClick={CreateComponent} className="adminButton" variant="success" size="lg">
  	    Create Product
  	  </Button>
  	  <Button onClick={UpdateComponent} className="adminButton" variant="success" size="lg">
  	    Update Product
  	  </Button>
  	  <Button onClick={SetAnAdmin} className="adminButton" variant="success" size="lg">
  	    Set An Admin
  	  </Button>
  	  </Row>
  	  </Container>
  	</div>
  );
}