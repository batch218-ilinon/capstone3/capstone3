import {UserProvider} from '../UserContext'
import {useState, useEffect} from 'react';
import DashboardButton from './DashboardButton'
import SingleProductCard from './SingleProductCard'
import UpdateProduct from './UpdateProduct'

export default function DashboardComponent() {

	const [components, setComponents] = useState(<DashboardButton />); 
	const [key, setKey] = useState(null);
	const [updateKey, setUpdateKey] = useState(false);

	const [singleCardComponent, setSingleCardComponent] = useState(null);  

	useEffect(() => {
		if(key !== null){
			fetch(`${process.env.REACT_APP_API_URL}/product/${key}`)
			.then(res => res.json())
			.then(data => {
					return setSingleCardComponent(
						(updateKey)?
						<UpdateProduct key = {data._id} product = {data}/>
						:
						<SingleProductCard key = {data._id} product = {data}/>)
			})
		}
	}, [key])

	const singleCardtoUpdateForm = (product) => {
		setSingleCardComponent(<UpdateProduct key = {product._id} product = {product}/>)
	}

	return(
		<>
		<UserProvider value={{components, setComponents, key, setKey, updateKey, setUpdateKey, singleCardComponent,setSingleCardComponent, singleCardtoUpdateForm}}>
		{singleCardComponent}
		{components}
		</UserProvider>
		</>
	)

}