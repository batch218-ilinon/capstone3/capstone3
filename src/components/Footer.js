import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

export default function Footer() {
	return(
		<Container fluid className="p-3 footer text-light">
			<Row>
				<Col className="text-center">© 2023  Ecommerce. All Rights Reserved.</Col>
			</Row>
		</Container>
	)
}
