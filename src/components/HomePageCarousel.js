import Carousel from 'react-bootstrap/Carousel';
import {useEffect, useState} from 'react';

import image1 from '../images/windows11.jpeg';
import image2 from '../images/intel.jpg';
import image3 from '../images/amd.jpg';
import image4 from '../images/windows11md.jpeg';
import image5 from '../images/intelC.jpg';
import image6 from '../images/amdC.jpg';

export default function HomePageCarousel() {

  return (
    <>
    <Carousel className="pb-5 d-none d-lg-block">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image1}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image2}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image3}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>

    <Carousel className="pb-5 d-lg-none">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image4}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image5}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={image6}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
    </>
  );
}
