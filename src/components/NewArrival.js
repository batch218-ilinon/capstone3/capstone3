import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import ProductCard from './ProductCard'

export default function TopSelling() {

	const [newItems, setNewItems] = useState([]);
	const {user} = useContext(UserContext);

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/product/newest`, {
		  method: 'GET',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  }
		})
		.then(res => res.json())
		.then(data => {
			const newProducts = data.slice(0, 8)
			setNewItems(newProducts.map(product => {
				if(user.isAdmin === undefined){
					product.userStatus = null
				}
				else{
					product.userStatus = user.isAdmin
				}
				return <ProductCard key={product._id} product = {product}/>
			}))
		})

	}, [])

	return (
		<>
		<h1 className="text-center mt-5 newArrivalHeader">New Arrival</h1>
		<Container className="mb-5">
			<Row className="justify-content-center">
				{newItems}
			</Row>
		</Container>
		</>
	)
}