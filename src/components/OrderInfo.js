import Accordion from 'react-bootstrap/Accordion';
import {useEffect} from 'react';

export default function OrderInfo({order}) {


  return (
    <>
    <Accordion className="mb-2">
      <Accordion.Item eventKey="0">
        <Accordion.Header>{order.email}-------({order.purchasedOn})</Accordion.Header>
        <Accordion.Body>
          userId: {order.userId}
        </Accordion.Body>
        <Accordion.Body>
          Products: {JSON.stringify(order.products)}
        </Accordion.Body>
        <Accordion.Body>
          Total amount: PHP {order.totalAmount}
        </Accordion.Body>
        <Accordion.Body>
          Date of purchase: {order.purchasedOn}
        </Accordion.Body>
      </Accordion.Item>
    </Accordion>
    </>
  );
}