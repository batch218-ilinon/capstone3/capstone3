import OrderInfo from './OrderInfo'
import DashboardButton from './DashboardButton'

import Button from 'react-bootstrap/Button';
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {useState, useContext} from 'react';
import UserContext from '../UserContext';


export default function OrderList() {
  const [orders, setOrders] = useState([])
  const {setComponents} = useContext(UserContext);
  
  function getOrders(){
    fetch(`${process.env.REACT_APP_API_URL}/users/orders`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setOrders(data.map(order => { 
        return <OrderInfo key={order._id} order = {order}/>
      }))
    })
  }

  getOrders()

  function returnDashboard(){
    setComponents(<DashboardButton />)
  }

  return (
    <Container className="ordersContainer">
      <h3 className="m-5 pt-3 pb-5 border-bottom border-light">Orders</h3>
      <Row className="justify-content-center">
        <Col xs={8}>
    {orders}
    <Row className="justify-content-end">
    <Button className="orderBackDashBoardButton mt-5" onClick={returnDashboard} variant="info" type="button" id="cancelBtn">
      Back
    </Button>
        </Row>
        </Col>
      </Row>
    </Container>
  );
}