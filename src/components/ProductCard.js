
import {useState, useContext} from 'react';
import {useNavigate} from 'react-router-dom';
import UserContext from '../UserContext';
import imageHolder from '../images/productPhoto.jpg';
import Col from 'react-bootstrap/Col';
import UserSingleProduct from './UserSingleProduct'
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Card from 'react-bootstrap/Card';
import Swal from 'sweetalert2';

export default function ProductCard({product}) {

  const navigate = useNavigate();
  const [count, setCount] = useState(1);
  const {setSingleProductComponent} = useContext(UserContext);

  function addCount(){
    setCount(count + 1)
  }

  function subtractCount(){
    (count === 1)?
    setCount(count)
    :
    setCount(count - 1)
  }

  function addToCart(){
    if(product.userStatus === null){
      navigate('/login')
    }
    else{
      fetch(`${process.env.REACT_APP_API_URL}/cart/${product._id}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('item')}`
        },
        body: JSON.stringify({
          "quantity": count
        })
      })  
      .then(res => res.json())
      .then(data => {
        if(data === true){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Added to Cart',
            showConfirmButton: false,
            timer: 1500
          })
          setCount(1)
        }
        else{
          alert("Error")
        }

      })
    }
  }

  function createOrder(){
    if(product.userStatus === null){
      navigate('/login')
    }
    else{
      fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('item')}`
        },
        body: JSON.stringify([{
          "productId": product._id,
          "quantity": count
        }])
      })  
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if(data === true){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Order Created',
            showConfirmButton: false,
            timer: 1500
          })
          setCount(1)
        }
        else{
          alert("Error")
        }
      })
    }
  }

  function updateSingleProduct(){
    setSingleProductComponent(<UserSingleProduct key = {product._id} product = {product}/>)
  }

  return (
    <Col xs={12} sm={8} md={6} lg={4} xl={3} className="p-3">
    <Card className="classCard" onClick={updateSingleProduct} style={{ width: '18rem' }}>
      <Card.Body>
        <Card.Img variant="top" src={imageHolder} />
        <Card.Title>{product.name}</Card.Title>
        <Card.Subtitle className="mb-2 text-muted ">PHP {product.price}</Card.Subtitle>
        <Card.Text>
          {product.description}
        </Card.Text>
        <ButtonGroup aria-label="Basic example" size="sm">
          <Button onClick={addCount} variant="secondary">+</Button>
          {count}
          <Button onClick={subtractCount} variant="secondary">-</Button>
        </ButtonGroup>
        <Button className="mx-1" onClick={createOrder} variant="info">Buy</Button> 
        <Button className="mx-1" onClick={addToCart} variant="info">Add to Cart</Button>
      </Card.Body>
    </Card>
    </Col>
  );
}