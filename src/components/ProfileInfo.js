import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import {useContext, useState, useEffect} from 'react';

import UserContext from '../UserContext';

export default function ProfileInfo({user}) {

	const {changePass} = useContext(UserContext);
	console.log(user.firstName)
	return (
		<>
			<div className="text-end changePasswordLink" onClick={changePass}>Change password</div>
			<Row className="justify-content-center">
			<Col xs={8}>
			<h5 className="m-5 pt-3 ">Name</h5>
			<p className="pb-3 text-center border-bottom border-light">{user.firstName} {user.lastName}</p>
			<h5 className="m-5 pt-3 ">Email</h5>
			<p className="pb-3 text-center border-bottom border-light">{user.email}</p>
			<h5 className="m-5 pt-3 ">Mobile No.</h5>
			<p className="pb-3 text-center border-bottom border-light">{user.mobileNo[0]}{user.mobileNo[1]}{user.mobileNo[2]}{user.mobileNo[3]}*******</p>
			</Col>
			</Row>
		</>
	)
}
