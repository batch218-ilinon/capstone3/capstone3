import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Swal from 'sweetalert2';

import DashboardButton from './DashboardButton'

import {useState, useContext, useEffect} from 'react';
import UserContext from '../UserContext';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

export default function SetAdmin() {
  const [email, setEmail] = useState("");
  const {setComponents} = useContext(UserContext);
  const [isActive, setIsActive] = useState(false);

  useEffect(() => {
      if(email !== ''){
                  setIsActive(true);
              } else {
                  setIsActive(false);
              }

  }, [email])

  function SetAsAdmin(e){
    e.preventDefault()
    fetch(`${process.env.REACT_APP_API_URL}/users/SetAsAdmin`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      },
      body: JSON.stringify({
        "email": email,
      })
    })
    .then(res => res.json())
    .then(data => {
      console.log(data)
      if(data === true){
        Swal.fire({
        title: "Success",
        icon: "success",
        text: `${email} set as an admin.`
        });
        setEmail("");
      }
      else{
        Swal.fire({
        title: "Error",
        icon: "error",
        text: "Error"
        })
      }
    })
  }

  function returnDashboard(){
    setComponents(<DashboardButton />)
  }

  return (
  <Container className="setAdmin">
    <h3 className="setAdminHeading m-5 pt-3 pb-5 border-bottom border-light">Set An Admin</h3>
    <Row className="justify-content-center">
    <Col xs={8}>
    <Form onSubmit = {SetAsAdmin}>
      <Form.Group className="mb-3" controlId="formBasicEmail">
        <Form.Label className="setAdminLabel">Email address</Form.Label>
        <Form.Control type="email" placeholder="Enter email" 
        value={email}
        onChange={e => setEmail(e.target.value)} 
        required/>
      </Form.Group>
      <Row className="justify-content-center">
      { isActive ?
      <Button variant="primary" type="submit" className="my-5 setAdmminButtons">
        Set As Admin
      </Button>
      :
      <Button variant="danger" type="submit" className="my-5 setAdmminButtons" disabled>
        Set As Admin
      </Button>
      }
      <Button onClick={returnDashboard} variant="primary" type="button" id="cancelBtn" className="my-3 setAdmminButtons">
        Cancel
      </Button>
      </Row>
    </Form>
    </Col>
    </Row>
  </Container>  
  );
}
