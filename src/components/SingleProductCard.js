import imageHolder from '../images/productPhoto.jpg';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import {useNavigate} from 'react-router-dom';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

export default function SingleProductCard({product}) {

  const {singleCardtoUpdateForm} = useContext(UserContext);
  const {user} = useContext(UserContext);
  const features = product.features.split("\n")

  const [productFeatures, setProductFeatures] = useState([]);

  useEffect(() => {

    setProductFeatures(features.map(feature => {
      return (
        <div>
        {feature}
        </div>
      )
    }))  
  }, [])

  function updateComponentToForm(){
    singleCardtoUpdateForm(product)
  }

  return (
    <Container className ="border border-primary my-3" >
        <Row className ="justify-content-center align-items-center">
            <Col>
                <Card.Img variant="top" src={imageHolder} />
            </Col>
            <Col>
                <Card className ="my-3">
                  <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    {(product.isActive)?
                    <Card.Subtitle className="text-muted">Active</Card.Subtitle>
                    :
                    <Card.Subtitle className="text-muted">Inactive</Card.Subtitle>}
                    <Card.Subtitle className="mb-2 text-muted">PHP {product.price}</Card.Subtitle>
                    <Card.Text>
                      {product.description}
                    </Card.Text>
                    <Card.Text>
                      <h5>Features</h5>  
                      <p>{productFeatures}</p>
                    </Card.Text>
                  </Card.Body>
                  <Button onClick={(updateComponentToForm)} variant="info">Update</Button>
                </Card>
            </Col>
        </Row>
    </Container>
  );
}
