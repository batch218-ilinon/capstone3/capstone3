import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import ProductCard from './ProductCard'

export default function TopSelling() {

	const [topItems, setTopItems] = useState([]);
	const {user} = useContext(UserContext);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/top`, {
		  method: 'GET',
		  headers: {
		    'Content-Type': 'application/json',
		    Authorization: `Bearer ${localStorage.getItem('item')}`
		  }
		})
		.then(res => res.json())
		.then(data => {
			const topProducts = data.slice(0, 8)
			setTopItems(topProducts.map(product => {
				if(user.isAdmin === undefined){
					product.userStatus = null
				}
				else{
					product.userStatus = user.isAdmin
				}
				return <ProductCard key={product._id} product = {product}/> 
			}))
		})

	}, [])

	return (
		<>
		<h1 className="text-center mt-5 pt-5 topSellingHeader">Top Selling</h1>
		<Container className="mb-5">
			<Row className="justify-content-center">
				{topItems}
			</Row>
		</Container>
		</>
	)
}