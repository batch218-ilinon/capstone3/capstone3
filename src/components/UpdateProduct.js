import AdminCard from './AdminCard'

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Dropdown from 'react-bootstrap/Dropdown';

import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Swal from 'sweetalert2';

export default function UpdateProduct({product}) {

  const {setSingleCardComponent} = useContext(UserContext);
  const {key, setKey} = useContext(UserContext);
  const {setComponents} = useContext(UserContext);

  const [productName, setProductName] = useState(product.name);
  const [productDescription, setProductDescription] = useState(product.description);
  const [productFeatures, setProductFeatures] = useState(product.features);
  const [productPrice, setProductPrice] = useState(product.price);
  const [isActive, setIsActive] = useState(product.isActive);
  const [productState, setProductState] = useState("");

  useEffect(() => {
    setProductName(product.name)
    setProductDescription(product.description)
    setProductPrice(product.price)
    setIsActive(product.isActive)
    if(product.isActive){
        setProductState("Active")
    }
    else{
      setProductState("Inactive")
    }      
  }, [product])

  function updateProduct(e){
      e.preventDefault()
      fetch(`${process.env.REACT_APP_API_URL}/product/${key}/update`, {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('item')}`
        },
        body: JSON.stringify({
          "name": productName,
          "description": productDescription,
          "price": productPrice,
          "isActive": isActive
        })
      })
      .then(res => res.json())
      .then(data => {
        if(data === true){
          Swal.fire({
          title: "Success",
          icon: "success",
          text: "Product successfully created!"
          });
          refreshCards()
          closeUpdateForm()
        }
        else if(data === false){
          Swal.fire({
          title: "Error",
          icon: "error",
          text: "Error"
          })
        }
        else{
          Swal.fire({
          title: "Error",
          icon: "error",
          text: "Error"
          })
        }
      })
  }

  function selectState(itemKey){
    if(itemKey === "Active"){
      setIsActive(true)
    }
    else{
      setIsActive(false)
    }
    setProductState(itemKey) 
  }

  function closeUpdateForm(){
    setKey(null)
    setSingleCardComponent(null)
  }

  function refreshCards(){
    console.log("Here")
    setComponents(<AdminCard />)
  }

  return (
    <Container className="updateProductForm">
      <h3 className="updateFormHeading m-5 pt-3 pb-5 border-bottom border-light">Update Product</h3>
      <Row className="justify-content-center">
        <Col xs={8}>
          <Form onSubmit = {updateProduct}>
            <Form.Group className="mb-3" controlId="productName">
              <Form.Label className="setAdminLabel">Product Name</Form.Label>
              <Form.Control type="name" placeholder="Enter product name"
              value={productName}
              onChange={e => setProductName(e.target.value)} 
              required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="productDescription">
              <Form.Label className="setAdminLabel">Product Description</Form.Label>
              <Form.Control type="description" placeholder="Enter product description" 
              value={productDescription}
              onChange={e => setProductDescription(e.target.value)} 
              required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="productDescription">
              <Form.Label className="createFormLabel">Product Features</Form.Label>
              <Form.Control rows="7" as="textarea" aria-label="With textarea" placeholder="Enter Features" 
              value={productFeatures}
              onChange={e => setProductFeatures(e.target.value)} 
              required/>
            </Form.Group>

            <Form.Group className="mb-3" controlId="productPrice">
              <Form.Label className="setAdminLabel">Product Price</Form.Label>
              <Form.Control type="price" placeholder="Enter product price"
              value={productPrice}
              onChange={e => setProductPrice(e.target.value)} 
              required/>
            </Form.Group>
            <Dropdown onSelect={selectState} className="mb-3">
              <Dropdown.Toggle variant="success" id="dropdown-basic">
                {productState}
              </Dropdown.Toggle>

              <Dropdown.Menu>
                <Dropdown.Item eventKey="Active">Active</Dropdown.Item>
                <Dropdown.Item eventKey="Inactive">Inactive</Dropdown.Item>
              </Dropdown.Menu>
            </Dropdown>
            <Row className="justify-content-center">
            <Button variant="danger" type="submit" id="submitBtn" className="updateProductButtons my-5">
            Update
            </Button>
            <Button onClick={closeUpdateForm} variant="primary" type="button" id="cancelBtn" className="updateProductButtons">
            Cancel
            </Button>
            </Row>
          </Form>
        </Col>
      </Row>
    </Container>
  )
}
