import imageHolder from '../images/productPhoto.jpg';

import Card from 'react-bootstrap/Card';
import Button from 'react-bootstrap/Button';
import ButtonGroup from 'react-bootstrap/ButtonGroup';

import {useNavigate} from 'react-router-dom';

import {useState, useEffect} from 'react';
import UserContext from '../UserContext';

import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';

import Swal from 'sweetalert2';

export default function UserSingleProduct({product}) {

  const navigate = useNavigate();
  const features = product.features.split("\n")

  const [productFeatures, setProductFeatures] = useState([]);
    const [count, setCount] = useState(1);

  useEffect(() => {

    setProductFeatures(features.map(feature => {
      return (
        <div>
        {feature}
        </div>
      )
    }))  
  }, [])

  function addCount(){
    setCount(count + 1)
  }

  function subtractCount(){
    (count === 1)?
    setCount(count)
    :
    setCount(count - 1)
  }

  function addToCart(){
    (product.userStatus === null) ?
    navigate('/login')
    :
    fetch(`${process.env.REACT_APP_API_URL}/cart/${product._id}`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      },
      body: JSON.stringify({
        "quantity": count
      })
    })  
    .then(res => res.json())
    .then(data => {
      if(data === true){
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Added to Cart',
          showConfirmButton: false,
          timer: 1500
        })
        setCount(1)
      }
      else{
        alert("Error")
      }

    })
  }

  function createOrder(){
    if(product.userStatus === null){
      navigate('/login')
    }
    else{
      fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Bearer ${localStorage.getItem('item')}`
        },
        body: JSON.stringify([{
          "productId": product._id,
          "quantity": count
        }])
      })  
      .then(res => res.json())
      .then(data => {
        console.log(data)
        if(data === true){
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Order Created',
            showConfirmButton: false,
            timer: 1500
          })
          setCount(1)
        }
        else{
          alert("Error")
        }
      })
    }
  }

  return (
    <Container className  ="mb-5 userSingleCardWrapper p-3" >
        <Row className  ="justify-content-center align-items-center singleCardContent">
            <Col>
                <Card.Img variant="top" src={imageHolder} />
            </Col>
            <Col>
                <Row>
                <Card className ="my-3 cardBody">
                  <Col xs={12}>
                  <Card.Body>
                    <Card.Title>{product.name}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">PHP {product.price}</Card.Subtitle>
                    <Card.Text>
                      {product.description}
                    </Card.Text>
                    <Card.Text>
                      <h5>Features</h5>  
                      <p>{productFeatures}</p>
                    </Card.Text>
                  </Card.Body>
                  </Col>
                    <Row className="justify-content-center align-items-center">
                      <Col>
                      <Button className="mainAddButton" onClick={addCount} variant="secondary">+</Button>
                      {count}
                      <Button className="mainSubtractButton" onClick={subtractCount} variant="secondary">-</Button>
                      </Col>
                      <Col lg={6} xl={8}>
                      <Button className="mb-2 mainBuyButton" onClick={createOrder} variant="info">Buy</Button>
                      <Button className="mb-2 mainAddToCartButton" onClick={addToCart} variant="info">Add to Cart</Button>  
                      </Col>
                    </Row>  
                </Card>
                </Row>
            </Col>
        </Row>
    </Container>
  );
}
