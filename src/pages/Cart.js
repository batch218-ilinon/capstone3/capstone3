import CartComponent from '../components/CartComponent'
import {Navigate} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';

export default function Cart(){

	const {user} = useContext(UserContext);

	return (
		(user.id === null) ?
		<Navigate to = "/login" />
		:
		<>
		<CartComponent />
		</>
	);
}