import HomePageCarousel from '../components/HomePageCarousel'
import TopSelling from '../components/TopSelling'
import NewArrival from '../components/NewArrival'


import {Navigate} from 'react-router-dom';
import {useContext} from 'react';
import UserContext from '../UserContext';

export default function Home(){

	const {user} = useContext(UserContext);
	
	return (
		(user.isAdmin === true) ?
		<Navigate to = "/dashboard" />
		:
		<>
		<HomePageCarousel />
		
		<div className="homeComponents">
		<TopSelling />
		<NewArrival />
		</div>
		</>
	)
}