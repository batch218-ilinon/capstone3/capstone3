import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext';

import {Navigate, Link} from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import Swal from 'sweetalert2';

export default function Login() {

    const {user, setUser} = useContext(UserContext);
    const {setLoading} = useContext(UserContext);

    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");

    const [isActive, setIsActive] = useState(false);

    useEffect(() => {
        if( email !== '' && password !== ''){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

        }, [email, password])

    function loginUser (e){
      e.preventDefault()

      fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          "email": email,
          "password": password
        })
      })
      .then(res => res.json())
      .then(data => {
        if(data === false){
          Swal.fire({
            title: "Email is not yet registered",
            icon: "error",
            text: "Please verify email address"
          })
        }
        else if(typeof data.access == "undefined"){
          Swal.fire({
            title: data.message,
            icon: "error",
            text: "Please verify password"
          })
        }
        else{
          setLoading(true);
          localStorage.setItem('item', data.access);
          retrieveUserDetails(data.access);
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Login Successful',
            showConfirmButton: false,
            timer: 1500
          })
        }
      })

    }

    const retrieveUserDetails = async (token) => {
      await fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
        headers: {
          Authorization: `Bearer ${token}`
        }
        })
        .then(res => res.json())
        .then(data => {

          setUser({
            id: data._id,
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            mobileNo: data.mobileNo,
            isAdmin: data.isAdmin
          })
          setLoading(false);
        })
    }

  return (
    (user.id !== null) ?
    <Navigate to = "/" />
    :
    <Container className="loginWrapper">
      <h3 className="m-5 pt-3 pb-5 border-bottom border-light">Log In</h3>
      <Row className="justify-content-center">
        <Col xs={6}>
          <Form onSubmit={loginUser}>
            <Form.Group controlId="formBasicEmail">
              <Form.Label >Email address</Form.Label>
              <Form.Control type="email" placeholder="Enter email" 
              value={email}
              onChange={e => setEmail(e.target.value)} 
              required/>
            </Form.Group>

            <Form.Group  className="mb-3" controlId="formBasicPassword">
              <Form.Label className="mt-5">Password</Form.Label>
              <Form.Control type="password" placeholder="Password" 
              value={password}
              onChange={e => setPassword(e.target.value)} 
              required/>
            </Form.Group>
            <Row className="justify-content-end">
            { isActive ? 
            <Button className="mt-5 signInButton" variant="primary" type="submit" id="submitBtn">
            Sign In
            </Button>
            :
            <Button className="mt-5 signInButton" variant="primary" type="submit" id="submitBtn" disabled>
            Sign In
            </Button>
            }
            </Row>
          </Form>
          <div className="text-center pt-5">No account yet? <Link to={`/register`}>Sign Up Here</Link> </div>
        </Col>
      </Row>
    </Container>
  );
}