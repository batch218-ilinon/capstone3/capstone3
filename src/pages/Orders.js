import OrderInfo from '../components/OrderInfo'
import Col from 'react-bootstrap/Col';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import {useState, useContext} from 'react';

export default function Orders() {
  const [orders, setOrders] = useState([])
  
  function getOrders(){
    fetch(`${process.env.REACT_APP_API_URL}/users/myOrders`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${localStorage.getItem('item')}`
      }
    })
    .then(res => res.json())
    .then(data => {
      setOrders(data.map(order => { 
        return <OrderInfo key={order._id} order = {order}/>
      }))
    })
  }

  getOrders()

  return (
    <Container className="ordersContainer">
      <h3 className="m-5 pt-3 pb-5 border-bottom border-light">Orders</h3>
      <Row className="justify-content-center">
        <Col xs={8}>
    {orders}
        </Col>
      </Row>
    </Container>
  );
}