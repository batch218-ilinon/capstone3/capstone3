import ProductCard from '../components/ProductCard'
import SingleProductCard from '../components/SingleProductCard'
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import {UserProvider} from '../UserContext'
import ButtonGroup from 'react-bootstrap/ButtonGroup';
import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Row';

export default function Products() {

	const [userProduct, setUserProduct] = useState({
	  id: null,
	  isAdmin: null
	});

	const [token, setToken] = useState(localStorage.getItem('item'))
	const [products, setProducts] = useState([])
	const [singleProductComponent, setSingleProductComponent] = useState(null)
	const [sortBy, setSortBy] = useState("Sort By")

	useEffect(() => {

		if(token !== null){
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			    headers: {
			        Authorization: `Bearer ${localStorage.getItem('item')}`
			    }
			})
			.then(res => res.json())
			.then(data => {
				console.log(data.isAdmin)
			    setUserProduct({
			        id: data._id,
			        firstName: data.firstName,
			        lastName: data.lastName,
			        email: data.email,
			        mobileNo: data.mobileNo,
			        isAdmin: data.isAdmin
			    })	
			})
		}
	}, []);

    useEffect(() => {
    	fetch(`${process.env.REACT_APP_API_URL}/product/active`)
    	.then(res => res.json())
    	.then(data => {

    		setProducts(data.map(product => {
    			console.log(userProduct.isAdmin)
    			product.userStatus = userProduct.isAdmin
    			return <ProductCard key={product._id} product = {product}/>
    		}))
    	})
    }, [userProduct])

    function selectSort(event){
    	if(event === "Best Sellers"){
    		setSortBy(event)
    		fetch(`${process.env.REACT_APP_API_URL}/product/top`, {
    		  method: 'GET',
    		  headers: {
    		    'Content-Type': 'application/json',
    		    Authorization: `Bearer ${localStorage.getItem('item')}`
    		  }
    		})
    		.then(res => res.json())
    		.then(data => {
    			setProducts(data.map(product => {
    				return <ProductCard key={product._id} product = {product}/> 
    			}))
    		})
    	}
    	else if(event === "Newest"){
    		setSortBy(event)
    		fetch(`${process.env.REACT_APP_API_URL}/product/newest`, {
    		  method: 'GET',
    		  headers: {
    		    'Content-Type': 'application/json',
    		    Authorization: `Bearer ${localStorage.getItem('item')}`
    		  }
    		})
    		.then(res => res.json())
    		.then(data => {
    			setProducts(data.map(product => {
    				return <ProductCard key={product._id} product = {product}/> 
    			}))
    		})
    	}
    	else if(event === "Price, low to high"){
    		setSortBy(event)
    		fetch(`${process.env.REACT_APP_API_URL}/product/lowest`, {
    		  method: 'GET',
    		  headers: {
    		    'Content-Type': 'application/json',
    		    Authorization: `Bearer ${localStorage.getItem('item')}`
    		  }
    		})
    		.then(res => res.json())
    		.then(data => {
    			setProducts(data.map(product => {
    				return <ProductCard key={product._id} product = {product}/> 
    			}))
    		})
    	}
    	else{
    		setSortBy(event)
    		fetch(`${process.env.REACT_APP_API_URL}/product/highest`, {
    		  method: 'GET',
    		  headers: {
    		    'Content-Type': 'application/json',
    		    Authorization: `Bearer ${localStorage.getItem('item')}`
    		  }
    		})
    		.then(res => res.json())
    		.then(data => {
    			setProducts(data.map(product => {
    				return <ProductCard key={product._id} product = {product}/> 
    			}))
    		})
    	}

    }
	return (
		<>
		<UserProvider value={{setSingleProductComponent, userProduct}}>
		<Container className="my-5">
				{singleProductComponent}
			<Row className="justify-content-end">
				<DropdownButton
				  as={ButtonGroup}
				  size="lg"
				  variant="warning"
				  title={sortBy} 
				  onSelect={selectSort}
				  drop={'down-centered'}
				  className="sortButton"
				>
				  <Dropdown.Item eventKey="Best Sellers">Best Sellers</Dropdown.Item>
				  <Dropdown.Item eventKey="Newest">Newest</Dropdown.Item>
				  <Dropdown.Item eventKey="Price, low to high">Price, low to high</Dropdown.Item>
				  <Dropdown.Divider />
				  <Dropdown.Item eventKey="Price, high to low">Price, high to low</Dropdown.Item>
				</DropdownButton>
			</Row>
			<Row>
				{products}
			</Row>
		</Container>
		</UserProvider>
		</>
	)
}