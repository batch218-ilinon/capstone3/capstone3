import {useContext, useState} from 'react';
import UserContext from '../UserContext';

import {UserProvider} from '../UserContext'
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

import ProfileInfo from '../components/ProfileInfo'
import ChangePassComponent from '../components/ChangePassComponent'

export default function Profile() {
    const {user} = useContext(UserContext);
    const [components, setComponents] = useState(<ProfileInfo key = {user.id} user = {user} />); 

    function changePass(){
    	setComponents(<ChangePassComponent />)
    }

    function confirmChange(){
    	setComponents(<ProfileInfo key = {user.id} user = {user}/>)
    }

	return (
		<>
		<UserProvider value={{changePass, confirmChange}}>
		<Container className="registerWrapper mt-5">
			<h3 className="m-5 pt-3 pb-5 border-bottom border-light">My Profile</ h3>
			{components}
		</Container>
		</UserProvider>
		</>
	)
}
