import {useState, useEffect, useContext} from 'react';

import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import {Navigate} from 'react-router-dom';
import UserContext from '../UserContext';

import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);

	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [email, setEmail] = useState("");
	const [mobileNo, setMobileNo] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");

	const [isActive, setIsActive] = useState(false);

	const [matchMessage, setMatchMessage] = useState("");

	useEffect(() => {
	    if(password1 !== password2){
	    	setMatchMessage("Password and Confirm password doesn't match")
	    }
	    else{
	    	setMatchMessage("")
	    }

	}, [password2])

	useEffect(() => {
	    if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
	                setIsActive(true);
	            } else {
	                setIsActive(false);
	            }

	    }, [firstName, lastName, email, password1, password2])


	function registerUser(e){
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
		  method: 'POST',
		  headers: {
		    'Content-Type': 'application/json'
		  },
		  body: JSON.stringify({
		    "firstName": firstName,
		    "lastName": lastName,
		    "email": email,
		    "mobileNo": mobileNo,
		    "password": password1
		  })
		})
		.then(res => res.json())
		.then(data => {

			if(data === false){
				Swal.fire({
				title: "Error",
				icon: "error",
				text: "Error"
				})
			}
			else if(data === true){
				Swal.fire({
				  position: 'center',
				  icon: 'success',
				  title: 'Thank you for registering!',
				  showConfirmButton: false,
				  timer: 1500
				})
				setFirstName("");
				setLastName("");
				setEmail("");
				setMobileNo("");
				setPassword1("");
				setPassword2("");
			}
			else{
				Swal.fire({
				title: "Error",
				icon: "error",
				text: (data.message)
				})
			}
		})
	}
  return (
  	(user.id !== null) ?
  	<Navigate to = "/products" />
  	:
  	<>
  	<Container className="registerWrapper mt-5">
  	<h3 className="m-5 pt-3 pb-5 border-bottom border-light">Create an Account</h3>
  		<Row className="justify-content-center">
  			<Col xs={6}>
		    <Form onSubmit={registerUser}>
		    	<Form.Group controlId="userFirstName">
		    	  <Form.Label>First Name</Form.Label>
		    	  <Form.Control type="name" placeholder="Enter first name"
		    	   value={firstName}
		    	   onChange={e => setFirstName(e.target.value)} 
		    	   required/>
		    	</Form.Group>

		    	<Form.Group className="pt-3" controlId="userLastName">
		    	  <Form.Label >Last Name</Form.Label>
		    	  <Form.Control type="name" placeholder="Enter last name" 
		    	  value={lastName}
		    	  onChange={e => setLastName(e.target.value)} 
		    	  required/>
		    	</Form.Group>

		      <Form.Group  className="pt-3" controlId="userEmail">
		        <Form.Label >Email address</Form.Label>
		        <Form.Control type="email" placeholder="Enter email" 
		        value={email}
		        onChange={e => setEmail(e.target.value)} 
		        required/>
		      </Form.Group>

		      <Form.Group  className="pt-3" controlId="userMobileNumber">
		        <Form.Label >Mobile No.</Form.Label>
		        <Form.Control type="number" placeholder="Enter mobile number" 
		        value={mobileNo}
		        onChange={e => setMobileNo(e.target.value)} 
		        required/>
		      </Form.Group>

		      <Form.Group className="pt-3" controlId="userPassword1">
		        <Form.Label >Password</Form.Label>
		        <Form.Control type="password" placeholder="Enter password" 
		        value={password1}
		        onChange={e => setPassword1(e.target.value)} 
		        required/>
		      </Form.Group>

		      <Form.Group className="pt-3" controlId="userPassword2">
		        <Form.Label >Confirm Password</Form.Label>
		        <Form.Control type="password" placeholder="Enter password again" 
		        value={password2}
		        onChange={e => setPassword2(e.target.value)} 
		        required/>
		      </Form.Group>

		      <Form.Text className="text-muted">
		      <div>
		        {matchMessage}
		      </div>
		      </Form.Text>
		      { isActive ? 
		      <Button className="mt-5" variant="primary" type="submit" id="submitBtn">
		      Sign up
		      </Button>
		      :
		      <Button className="mt-5" variant="primary" type="submit" id="submitBtn" disabled>
		      Sign up
		      </Button>
		      }
		    </Form>
		    </Col>
    	</Row>
    </Container>
    </>
  );
}